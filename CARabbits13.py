import numpy, random
from CA import CA

class CARabbits(CA):
    """
    Rabbits weeds grass CA, implemention of original model
    with rearranged color channel layout.
        
    RedChannel: Rabbit Age
    GreenChannel: Grass Type
    BlueChannel: Rabbit Energy
    AlphaChannel: Special
    """

    def __init__(self, width, height, rabbits=10, grass=50):
        self.width = width
        self.height = height
        self.channels = 4
        self.type = numpy.uint8

        self.vsc = """
            #version 150

            in vec4 vPosition;
            out vec4 fPosition;
             
            void main() {
                fPosition = vPosition;
                gl_Position = vPosition;
            }
        """;
        
        self.fsc = """
            #version 150

            in vec4 fPosition;
            out vec4 color;
            
            uniform sampler2D celldata;
            
            void main() {
                int max = 255;
                
                vec2 texcoord = vec2(fPosition.x + 1, -fPosition.y + 1) / 2;
                vec4 sample = texture(celldata, texcoord);
                
                ivec4 cell = ivec4(255 * sample);


                if(cell.r>0) {
                    if(cell.a == 8) color = vec4(1,0,1,1);
                    else color = vec4(1,1,1,1);
                }
                else if(cell.g>0) {
                    if(cell.g==1) color = vec4(0,1,0,1);
                    else          color = vec4(1,1,0,1);
                }
                else if (cell.b>0) color = vec4(1,1,0,1);
                else color = vec4(0,0,0,1);
            }
        """

        self.total_rabbits = 0
        self.total_grass = 0
        self.total_weeds = 0

        self.grass_growth = 20
        self.weed_growth = 5

        self.grass_energy = 20
        self.weed_energy = 3

        self.birth_threshold = 40

        self.min_start_energy = 10
        self.max_start_energy = 10

        self.max_rabbit_age = 16
        self.reset()

    def controls(self):
        return [
            {'name': 'Max Rabbit Age', 'attr': 'max_rabbit_age'},
            {'name': 'Grass growth', 'attr': 'grass_growth'},
            {'name': 'Weed growth', 'attr': 'weed_growth'},
            {'name': 'Grass Energy', 'attr': 'grass_energy'},
            {'name': 'Weed Energy', 'attr': 'weed_energy'},
            {'name': 'Min Start Energy', 'attr': 'min_start_energy'},
            {'name': 'Max Start Energy', 'attr': 'max_start_energy'},
            {'name': 'Birth Threshold', 'attr': 'birth_threshold'}
        ]

    def trackers(self):
        return [
            {'name': 'rabbits', 'attr': 'total_rabbits', 'color': 'red'},
            {'name': 'grass', 'attr': 'total_grass', 'color': 'green'},
            {'name': 'weeds', 'attr': 'total_weeds', 'color': 'orange'}]
        
    def get_cell(self, x, y):
        return self.cells[y % self.height, x % self.width, :]

    def get_channel(self, x, y, z):
        return self.cells[y % self.height, x % self.width, z]

    def set_cell(self, x, y, value):
        self.nextcells[y % self.height, x % self.width, :] = value

    def set_channel(self, x, y, z, value):
        self.nextcells[y % self.height, x % self.width, z] = value

    def get_next_channel(self, x, y, z):
        return self.nextcells[y % self.height, x % self.width, z]

    def reset(self, rabbits=10, grass=120):
        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=numpy.uint8)
        self.nextcells = numpy.zeros((self.height, self.width, self.channels), dtype=numpy.uint8)

        for i in range(rabbits):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            self.cells[y, x, 0] = 1
        
        for i in range(grass):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            self.cells[y, x, 1] = 1

    def step(self):
        """Update next_cells from cells, then swap and clear the previous frame"""

        self.total_rabbits = 0
        self.total_grass = 0
        self.total_weeds = 0

        # copy grass
        for x in range(self.width):
            for y in range(self.height):
                grass = self.get_channel(x, y, 1)
                if grass > 0:
                    if grass == 1:
                        self.total_grass += 1
                    if grass == 2:
                        self.total_weeds += 1
                    self.set_channel(x, y, 1, grass)

        # grow grass
        for i in range(0, self.grass_growth):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            self.set_channel(x, y, 1, 1)
        
        # grow weeds
        for i in range(0, self.weed_growth):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            self.set_channel(x, y, 1, 2)
            
        # Alter rabbits
        for x in range(self.width):
            for y in range(self.height):
                rabbit = int(self.get_channel(x,y,0))
                energy = int(self.get_channel(x,y,2))
                if rabbit > 0:         
                    self.total_rabbits += 1

                    # age rabbit
                    rabbit += 1
                    if rabbit > self.max_rabbit_age:
                        continue
                    
                    # hunger rabbit
                    #energy = max(0, energy - 1)
                    #if energy == 0: continue
                    
                    directions = [0]
                    
                    # detect if rabbit is already in an adjacent space
                    if (self.get_channel(x + 1, y, 0) == 0) & (self.get_next_channel(x + 1, y, 0) == 0):
                        directions.append(1)
                    if (self.get_channel(x - 1, y, 0) == 0) & (self.get_next_channel(x - 1, y, 0) == 0):
                        directions.append(2)
                    if (self.get_channel(x, y + 1, 0) == 0) & (self.get_next_channel(x, y + 1, 0) == 0):
                        directions.append(3)
                    if (self.get_channel(x, y - 1, 0) == 0) & (self.get_next_channel(x, y - 1, 0) == 0):
                        directions.append(4)
                    
                    direction = random.choice(directions)
                    
                    dir = [x, y]
                    if (direction == 1): dir = [x + 1, y]
                    if (direction == 2): dir = [x - 1, y]
                    if (direction == 3): dir = [x, y + 1]
                    if (direction == 4): dir = [x, y - 1]
                    
                    
                    #rabbit feed
                    grass = self.get_channel(dir[0], dir[1], 1)
                    
                    if grass > 0:
                        if grass == 1: 
                            energy = min(255, energy + self.grass_energy)
                        if grass == 2:
                            energy = min(255, energy + self.weed_energy)
                        
                        self.set_channel(dir[0], dir[1], 1, 0) # remove grass
                        
                        if energy >= self.birth_threshold:
                            energy = 1
                        
                            # add breed flag
                            self.set_channel(dir[0], dir[1], 3, 8)

                            # numpy.random.random_integers(self.min_start_energy, self.max_start_energy)

                            # create new rabbits
                            self.set_channel(dir[0] + 1, dir[1], 0, 1)
                            self.set_channel(dir[0]+1, dir[1], 2,
                                             numpy.random.random_integers(self.min_start_energy, self.max_start_energy, 1))

                            self.set_channel(dir[0] - 1, dir[1], 0, 1)
                            self.set_channel(dir[0]-1, dir[1], 2,
                                             numpy.random.random_integers(self.min_start_energy, self.max_start_energy, 1))

                            self.set_channel(dir[0], dir[1]+1, 0, 1)
                            self.set_channel(dir[0], dir[1]+1, 2,
                                             numpy.random.random_integers(self.min_start_energy, self.max_start_energy, 1))

                            self.set_channel(dir[0], dir[1]-1, 0, 1)
                            self.set_channel(dir[0], dir[1]-1, 2,
                                             numpy.random.random_integers(self.min_start_energy, self.max_start_energy, 1))
                            
                    self.set_channel(dir[0], dir[1], 0, rabbit) # move rabbit
                    self.set_channel(dir[0], dir[1], 2, energy)

        # swap
        self.cells, self.nextcells = self.nextcells, self.cells
        self.nextcells.fill(0)

if __name__ == "__main__":    
    from CAViewerQt4 import CAViewerQt4
    ca = CARabbits(64,64)
    viewer = CAViewerQt4(ca)
    viewer.run()