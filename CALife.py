
import numpy
import scipy.ndimage #importing this causes an error at close...
from CA import CA

class CALife(CA):
    """Game of Life Cellular Automata"""
    
    def __init__(self, width, height, random=False):
        self.width = width
        self.height = height
        self.channels = 1
        self.type = numpy.uint8
        
        self.weights = numpy.array(([1,1,1],
                                    [1,10,1],
                                    [1,1,1]))
        
        self.mode = "wrap"
        
        self.vsc = """
            #version 150

            in vec4 vPosition;
            out vec4 fPosition;
             
            void main() {
                fPosition = vPosition;
                gl_Position = vPosition;
            }
        """;
        
        self.fsc = """
            #version 150

            in vec4 fPosition;
            out vec4 color;
            
            uniform sampler2D celldata;
            
            void main() {
                int max = 255;
                int rabbitlife = 32;
                
                vec2 texcoord = vec2(fPosition.x + 1, -fPosition.y + 1) / 2;
                vec4 sample = texture(celldata, texcoord);
                
                ivec4 cell = ivec4(255 * sample);
                
                if(cell.x==0)       color = vec4(0,0,0,1);
                else                color = vec4(1, 1, 1, 1);
            }
        """;
        
        
        self.reset(random)

    def reset(self, random=True):
        #if random:
        self.cells = numpy.random.random_integers(0, 1, (self.height, self.width, self.channels)).astype(numpy.uint32)
        #else:
        #    self.cells = numpy.ones((self.width, self.height), dtype=numpy.uint8)
    
    def step(self):
        con = scipy.ndimage.filters.convolve(self.cells[:,:,0], self.weights, mode=self.mode)
        value = (con==3) | (con==12) | (con==13)
        self.cells[:,:,0] = numpy.int32(value)
