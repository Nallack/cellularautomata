

import numpy
import matplotlib
import matplotlib.pyplot as pyplot

class CAViewer(object):
    """CA"""
    
    def run(self, steps=100):
        print "run NotImplemented"
        #raise NotImplementedError
        

        
class CAViewerPyplot(CAViewer):
    """Animates Cellular Automata using pyplot"""
    def __init__(self, ca):
        self.ca = ca
        self.cmap = matplotlib.cm.gray_r
        
        self.figure = pyplot.figure()
        pyplot.axis([0, ca.width, 0, ca.height])
        pyplot.xticks([])
        pyplot.yticks([])
        self.pcolor = None
        self.display()
        
    def display(self):
        """update the display"""
        if (self.pcolor):
            self.pcolor.remove()
            
        self.pcolor = pyplot.pcolor(self.ca.cells, cmap=self.cmap)
        
        self.figure.canvas.draw()
    
    def animate_callback(self):
        for i in range(self.steps):
            self.ca.step()
            self.display()
    
    def run(self, steps=10):
        self.steps = steps
        self.figure.canvas.manager.window.after(1000, self.animate_callback)
        pyplot.show()