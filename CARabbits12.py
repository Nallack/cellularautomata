import numpy, random
from CA import CA


class CARabbits(CA):
    """
    Rabbits weeds grass CA 
        
    RedChannel: Cell Type
    GreenChannel: Cell Life/Decay
    BlueChannel: Cell Health/Birth
    """

    def __init__(self, width, height, rabbits=10, grass=50):
        self.width = width
        self.height = height
        self.channels = 4
        self.type = numpy.uint8

        self.vsc = """
            #version 150

            in vec4 vPosition;
            out vec4 fPosition;
             
            void main() {
                fPosition = vPosition;
                gl_Position = vPosition;
            }
        """;
        
        self.fsc = """
            #version 150

            in vec4 fPosition;
            out vec4 color;
            
            uniform sampler2D celldata;
            
            void main() {
                int max = 255;
                int rabbitlife = 32;
                
                vec2 texcoord = vec2(fPosition.x + 1, -fPosition.y + 1) / 2;
                vec4 sample = texture(celldata, texcoord);
                
                ivec4 cell = ivec4(255 * sample);
                
                if(cell.r>0) { 
                    if(cell.a == 8) color = vec4(1,0,1,1);
                    else color = vec4(1,1,1,1);
                }
                else if(cell.g>0)  color = vec4(0,1,0,1);
                else if (cell.b>0) color = vec4(1,1,0,1);
                else color = vec4(0,0,0,1);
            }
        """;

        
        #tracking variables
        self.total_rabbits = 0
        self.total_grass = 0
        
        #config variables
        self.grass_growth = 20
        self.weed_growth = 10
        self.max_rabbit_age = 8

        self.reset()
    
    def set_grass_growth(self, value):
        self.grass_growth = value
        
    def set_max_rabbit_age(self, value):
        self.max_rabbit_age = value
    
    def controls(self):
        return [
            {'name': 'Max Rabbit Age', 'attr': 'max_rabbit_age', 'action': self.set_max_rabbit_age},
            {'name': 'Grass Growth', 'attr': 'grass_growth', 'action': self.set_grass_growth}
            ]
    
    def trackers(self):
        return [
            {'name': 'rabbits', 'attr': 'total_rabbits', 'color': 'red'},
            {'name': 'grass', 'attr': 'total_grass', 'color': 'green'}]
    
    def get_cell(self, x, y):
        return self.cells[y % self.height, x % self.width, :]

    def get_channel(self, x, y, z):
        return self.cells[y % self.height, x % self.width, z]

    def set_cell(self, x, y, value):
        self.nextcells[y % self.height, x % self.width, :] = value

    def set_channel(self, x, y, z, value):
        self.nextcells[y % self.height, x % self.width, z] = value

    def get_next_channel(self, x, y, z):
        return self.nextcells[y % self.height, x % self.width, z]

    def reset(self, rabbits=10, grass=120):
        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=numpy.uint8)
        self.nextcells = numpy.zeros((self.height, self.width, self.channels), dtype=numpy.uint8)

        for i in range(rabbits):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            self.cells[y, x, 0] = 1
        
        for i in range(grass):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            self.cells[y, x, 1] = 1


    def step(self):
        """Update next_cells from cells, then swap and clear the previous frame"""
        self.total_rabbits = 0
        self.total_grass = 0
        
        # copy grass
        for x in range(self.width):
            for y in range(self.height):
                grass = self.get_channel(x, y, 1)
                if grass > 0:
                    self.total_grass += 1
                    self.set_channel(x, y, 1, grass)

        # grow grass
        for i in range(0, self.grass_growth):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            if self.get_channel(x,y,0) == 0:
                self.set_channel(x, y, 1, 1)
            
        #Alter rabbits
        for x in range(self.width):
            for y in range(self.height):
                rabbit = self.get_channel(x,y,0)
                if rabbit > 0: 
                    self.total_rabbits +=1
                    
                    directions = [0]
                    
                    # detect if rabbit is already in an adjacent space
                    if (self.get_channel(x + 1, y, 0) == 0) & (self.get_next_channel(x + 1, y, 0) == 0):
                        directions.append(1)
                    if (self.get_channel(x - 1, y, 0) == 0) & (self.get_next_channel(x - 1, y, 0) == 0):
                        directions.append(2)
                    if (self.get_channel(x, y + 1, 0) == 0) & (self.get_next_channel(x, y + 1, 0) == 0):
                        directions.append(3)
                    if (self.get_channel(x, y - 1, 0) == 0) & (self.get_next_channel(x, y - 1, 0) == 0):
                        directions.append(4)
                    
                    direction = random.choice(directions)
                    
                    dir = [x, y]
                    if (direction == 1): dir = [x + 1, y]
                    if (direction == 2): dir = [x - 1, y]
                    if (direction == 3): dir = [x, y + 1]
                    if (direction == 4): dir = [x, y - 1]
                    
                    #eat grass
                    self.set_channel(dir[0], dir[1], 0, self.get_channel(x,y,0))
                    if self.get_channel(dir[0], dir[1],1) > 0:
                        #remove grass
                        self.set_channel(dir[0], dir[1], 1, 0)
                        self.set_channel(dir[0], dir[1], 3, 8)
                        
                    
        #breed rabbit        
        for x in range(self.width):
            for y in range(self.height):
                    if self.get_next_channel(x,y,3) == 8:
                            
                        #create new rabbits
                        self.set_channel(x+1, y, 0, 1)
                        self.set_channel(x-1, y, 0, 1)
                        self.set_channel(x, y+1, 0, 1)
                        self.set_channel(x, y-1, 0, 1)
        
        #drain rabbits
        for x in range(self.width):
            for y in range(self.height):
                #age rabbit
                rabbit = self.get_next_channel(x,y,0)
                if rabbit > 0:
                    if(rabbit > self.max_rabbit_age): 
                        self.set_channel(x, y, 0, 0)
                    else: 
                        self.set_channel(x, y, 0, rabbit + 1)

        # swap
        self.cells, self.nextcells = self.nextcells, self.cells
        self.nextcells.fill(0)

if __name__ == "__main__":    
    from CAViewerQt4 import CAViewerQt4
    ca = CARabbits(64,64)
    viewer = CAViewerQt4(ca)
    viewer.run()