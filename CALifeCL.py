
import pyopencl as cl
import numpy
import scipy.ndimage #importing this causes an error at close...
from CA import CA
import os


class CALife(CA):
    """Game of Life Cellular Automata"""
    
    def __init__(self, width, height, random=False):
        self.width = width
        self.height = height
        self.channels = 1
        self.type = numpy.uint8
        
        self.vsc = """
            #version 150

            in vec4 vPosition;
            out vec4 fPosition;
             
            void main() {
                fPosition = vPosition;
                gl_Position = vPosition;
            }
        """
        
        self.fsc = """
            #version 150

            in vec4 fPosition;
            out vec4 color;
            
            uniform sampler2D celldata;
            
            void main() {
                int max = 255;
                int rabbitlife = 32;
                
                vec2 texcoord = vec2(fPosition.x + 1, -fPosition.y + 1) / 2;
                vec4 sample = texture(celldata, texcoord);
                
                ivec4 cell = ivec4(255 * sample);
                
                if(cell.x==0)       color = vec4(0,0,0,1);
                else if(cell.x==1)  color = vec4(1,1,1,1);
                else color = vec4(1,0,0,1);
            }
        """
        
        #wrapping enabled
        self.clc = """
            uint index(x, y) {
                return (y%get_global_size(1)) * get_global_size(0) + (x%get_global_size(0));
            }
        
            kernel void main(global uchar* a, global uchar* b) {
                uint x = get_global_id(0);
                uint y = get_global_id(1);
                
                uint id = index(x, y);
                
                uchar sum = 0;
                sum += a[index(x-1,y-1)];
                sum += a[index(x-1,y  )];
                sum += a[index(x-1,y+1)];
                
                sum += a[index(x  ,y-1)];
                sum += a[index(x  ,y+1)];
                
                sum += a[index(x+1,y-1)];
                sum += a[index(x+1,y  )];
                sum += a[index(x+1,y+1)];
                
                
                if(a[id] == 0) {
                    if(sum == 3) b[id] = 1;
                    else b[id] = 0;
                }
                else {
                    if(sum<2) b[id] = 0;
                    else if(sum<4) b[id] = 1;
                    else b[id] = 0;
                }
                
            }
        """
        
        self.reset(random)
        
        # --- OPENCL SETUP -----
        os.environ['PYOPENCL_CTX'] = '0:0'  # use first available device
        self.context = cl.create_some_context()
        self.queue = cl.CommandQueue(self.context)
        self.program = cl.Program(self.context, self.clc).build()
        
        mf = cl.mem_flags
        self.buffer0 = cl.Buffer(self.context, mf.READ_ONLY, self.cells.nbytes)
        self.buffer1 = cl.Buffer(self.context, mf.WRITE_ONLY, self.cells.nbytes)

    def reset(self, random=True):
        self.cells = numpy.random.random_integers(0, 1, (self.height, self.width, self.channels)).astype(numpy.uint8)

    def step(self):
        #cl.enqueue_write_buffer(self.queue, self.buffer0, self.cells).wait()
        cl.enqueue_copy(self.queue, self.buffer0, self.cells)
        self.program.main(self.queue, self.cells.shape, None, self.buffer0, self.buffer1)
        #cl.enqueue_read_buffer(self.queue, self.buffer1, self.cells).wait()
        cl.enqueue_copy(self.queue, self.cells, self.buffer1)

if __name__ == "__main__":
    from CAViewerQt4 import CAViewerQt4
    ca = CALife(1024, 1024)
    viewer = CAViewerQt4(ca)
    viewer.run()
