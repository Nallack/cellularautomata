import numpy, random
from CA import CA


class CARabbits(CA):
    """
    Rabbits weeds grass CA 
        
    RedChannel: Cell Type
    GreenChannel: Cell Life/Decay
    BlueChannel: Cell Health/Birth
    """

    def __init__(self, width, height, rabbits=10, grass=50):
        self.width = width
        self.height = height
        self.channels = 3
        self.type = numpy.uint8

        self.vsc = """
            #version 150

            in vec4 vPosition;
            out vec4 fPosition;
             
            void main() {
                fPosition = vPosition;
                gl_Position = vPosition;
            }
        """;
        
        self.fsc = """
            #version 150

            in vec4 fPosition;
            out vec4 color;
            
            uniform sampler2D celldata;
            
            void main() {
                int max = 255;
                int rabbitlife = 32;
                
                vec2 texcoord = vec2(fPosition.x + 1, -fPosition.y + 1) / 2;
                vec4 sample = texture(celldata, texcoord);
                
                ivec4 cell = ivec4(255 * sample);
                float life = (rabbitlife - cell.y) / float(rabbitlife);
                
                if(cell.x==0)       color = vec4(0,0,0,1);
                else if(cell.x==1) {
                    if(cell.z==0) color = vec4(1, 1, 1, 1);
                    else color = vec4(1,0,1,1);
                }
                else if(cell.x==2)  color = vec4(0,1,0,1);
                else                color = vec4(1,0.5,0,1);
            }
        """;
        
        #tracking varaibles
        self.time_elapsed = 0
        self.total_rabbits = 0
        self.total_grass = 0
        
        #control variables
        self.grass_growth = 20
        self.rabbit_life = 8
        self.wonder = True  # rabbits move randomly
        self.reset()
        
    def controls(self):
        return [
            {'name': 'max rabbit age', 'attr': 'rabbit_life'},
            {'name': 'grass growth', 'attr': 'grass_growth'}]
    
    def trackers(self):
        return [
            {'name': 'rabbits', 'attr': 'total_rabbits', 'color': 'red'},
            {'name': 'grass', 'attr': 'total_grass', 'color': 'green'}]
    
    def get_cell(self, x, y):
        return self.cells[y % self.height, x % self.width, :]

    def get_channel(self, x, y, z):
        return self.cells[y % self.height, x % self.width, z]

    def getNextChannel(self, x, y, z):
        return self.nextcells[y % self.height, x % self.width, z]

    def setCell(self, x, y, value):
        self.nextcells[y % self.height, x % self.width, :] = value

    def setChannel(self, x, y, z, value):
        self.nextcells[y % self.height, x % self.width, z] = value

    def reset(self, rabbits=10, grass=120):
        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=numpy.uint8)
        self.nextcells = numpy.zeros((self.height, self.width, self.channels), dtype=numpy.uint8)

        for i in range(grass):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            if self.cells[y, x, 0] == 0: self.cells[y, x, 0] = 2

        for i in range(rabbits):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            if self.cells[y, x, 0] == 0: self.cells[y, x, 0] = 1

    def step(self):
        """Update next_cells from cells, then swap and clear the previous frame"""
        self.total_rabbits = 0
        self.total_grass = 0
        
        # copy grass
        for x in range(self.width):
            for y in range(self.height):
                if self.get_channel(x, y, 0) == 2:
                    self.total_grass += 1
                    self.setChannel(x, y, 0, 2)

        # grow grass
        for i in range(0, self.grass_growth):
            x = numpy.random.randint(0, self.width, 1)
            y = numpy.random.randint(0, self.height, 1)
            if self.get_channel(x, y, 0) == 0: self.setChannel(x, y, 0, 2)

        # SUGGESTION: may want to algorithmically move instead of random
        # rabbit wander
        for x in range(self.width):
            for y in range(self.height):
                if self.get_channel(x, y, 0) == 1:
                    directions = [0]

                    # detect if rabbit is already in an adjacent space
                    if (self.get_channel(x + 1, y, 0) != 1) & (self.getNextChannel(x + 1, y, 0) != 1):
                        directions.append(1)
                    if (self.get_channel(x - 1, y, 0) != 1) & (self.getNextChannel(x - 1, y, 0) != 1):
                        directions.append(2)
                    if (self.get_channel(x, y + 1, 0) != 1) & (self.getNextChannel(x, y + 1, 0) != 1):
                        directions.append(3)
                    if (self.get_channel(x, y - 1, 0) != 1) & (self.getNextChannel(x, y - 1, 0) != 1):
                        directions.append(4)

                    direction = random.choice(directions)

                    # Move and detect what is eaten
                    dir = [x, y]
                    if (direction == 1): dir = [x + 1, y]
                    if (direction == 2): dir = [x - 1, y]
                    if (direction == 3): dir = [x, y + 1]
                    if (direction == 4): dir = [x, y - 1]

                    self.setCell(dir[0], dir[1], self.get_cell(x, y))
                    self.setChannel(dir[0], dir[1], 2, 0)
                    if self.get_channel(dir[0], dir[1], 0) == 2:
                        self.setChannel(dir[0], dir[1], 2, 1)

        # breed rabbits
        for x in range(self.width):
            for y in range(self.height):
                if self.nextcells[y, x, 0] == 1 & self.nextcells[y, x, 2] == 1:
                    self.setChannel(x + 1, y, 0, 1)
                    self.setChannel(x - 1, y, 0, 1)
                    self.setChannel(x, y + 1, 0, 1)
                    self.setChannel(x, y - 1, 0, 1)

        
        # drainrabbits
        for x in range(self.width):
            for y in range(self.height):
                if self.nextcells[y, x, 0] == 1:
                    self.total_rabbits += 1 #tracking
                    if self.nextcells[y, x, 1] >= self.rabbit_life:
                        self.nextcells[y, x] = numpy.zeros(self.channels)
                    else:
                        self.nextcells[y, x, 1] += 1

        # swap
        self.cells, self.nextcells = self.nextcells, self.cells
        self.nextcells.fill(0)

if __name__ == "__main__":    
    from CAViewerQt4 import CAViewerQt4
    ca = CARabbits(64,64)
    viewer = CAViewerQt4(ca)
    viewer.run()