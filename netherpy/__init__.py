"""
This is a collection of OpenGL classes adapted from
another self-owned proprietary project.

* graphics - Graphics library

@copyright: 2016, Callan Gray <nallack@nethersoft.com.au>
@license: BSD, see LICENSE for details
"""

__version__ = '0.1'

