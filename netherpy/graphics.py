"""
This is a collection of OpenGL classes adapted from
another self-owned proprietary project.

@copyright: 2016, Callan Gray <nallack@nethersoft.com.au>
@license: BSD, see LICENSE for details
"""

import numpy
from OpenGL.GL import *
import ctypes

class Quad(object):
    def __init__(self):
        vertices = numpy.array([
            [-1,-1, 0, 1],
            [-1, 1, 0, 1],
            [ 1,-1, 0, 1],
            [ 1, 1, 0, 1]], dtype=numpy.float32);
        
        self.vaoid = glGenVertexArrays(1)
        glBindVertexArray(self.vaoid)
        self.vboid = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.vboid)
        
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, ctypes.c_void_p(0))
        
        glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW)
        
        glBindVertexArray(0)
    
    def bind(self):
        glBindVertexArray(self.vaoid)
    
    def draw(self):
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
    
    def unbind(self):
        glBindVertexArray(0)
    
    def dispose(self):
        glDeleteBuffers(1, self.vboid)
        glDeleteVertexArrays(1, self.vaoid)
        
class Texture2D(object):
    def __init__(self, width, height, data, channels=1, type=numpy.uint8):
        self.textureid = glGenTextures(1)
        self.internalformat = 0
        self.format = 0
        self.nptype = type
        if(type == numpy.uint8): self.type = GL_UNSIGNED_BYTE
        elif(type == numpy.uint16): self.type = GL_UNSIGNED_SHORT
        elif(type == numpy.uint32): self.type = GL_UNSIGNED_INT
        
        glBindTexture(GL_TEXTURE_2D, self.textureid)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0)
        glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        
        if(self.type == GL_UNSIGNED_BYTE):
            if(channels == 1): 
                self.format = GL_RED
                self.internalformat = GL_R8
            if(channels == 2):
                self.format = GL_RG
                self.internalformat = GL_RG8
            if(channels == 3):
                self.format = GL_RGB
                self.internalformat = GL_RGB8
            if(channels == 4):
                self.format = GL_RGBA
                self.internalformat = GL_RGBA8
        elif(self.type == GL_UNSIGNED_SHORT):
            if(channels == 1): 
                self.format = GL_RED
                self.internalformat = GL_R16
            if(channels == 2):
                self.format = GL_RG
                self.internalformat = GL_RG16
            if(channels == 3):
                self.format = GL_RGB
                self.internalformat = GL_RGB16
            if(channels == 4):
                self.format = GL_RGBA
                self.internalformat = GL_RGBA16
        elif(self.type == GL_UNSIGNED_INT):
            if(channels == 1): 
                self.format = GL_RED
                self.internalformat = GL_R32
            if(channels == 2):
                self.format = GL_RG
                self.internalformat = GL_RG32
            if(channels == 3):
                self.format = GL_RGB
                self.internalformat = GL_RGB32
            if(channels == 4):
                self.format = GL_RGBA
                self.internalformat = GL_RGBA32
        
        glTexImage2D(GL_TEXTURE_2D, 0, self.internalformat, width, height, 0, self.format, self.type, data)
        
        glBindTexture(GL_TEXTURE_2D, 0)
        
    def update(self, x, y, width, height, data):
        glBindTexture(GL_TEXTURE_2D, self.textureid)
        glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, self.format, self.type, data)
        glBindTexture(GL_TEXTURE_2D, 0)
    
    def bind(self, index):
        glActiveTexture(GL_TEXTURE0 + index)
        glBindTexture(GL_TEXTURE_2D, self.textureid)
    
    def dispose(self):
        glDeleteTextures(1, self.textureid)
    
class UniformBuffer(object):
    def __init__(self, data):
        self.uboid = glGenBuffers(1)
        glBindBuffer(GL_UNIFORM_BUFFER, self.uboid)
        glBufferData(GL_UNIFORM_BUFFER, data, GL_DYNAMIC_DRAW)
    
    def bind(self, index):
        glBindBufferBase(GL_UNIFORM_BUFFER, index, self.uboid)
        
    def update(self, offset, size, data):
        glBindBuffer(GL_UNIFORM_BUFFER, self.uboid)
        glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data)
        glBindBuffer(GL_UNIFORM_BUFFER, 0)
    
    def unbind(self, index):
        glBindBufferBase(GL_UNIFORM_BUFFER, index, 0)
        
    def dispose(self):
        glDeleteBuffers(1, self.uboid)
        
class Shader(object):
        def __init__(self, shadertype):
            self.shaderid = glCreateShader(shadertype)
        
        def compileFromString(self, source):
            glShaderSource(self.shaderid, source)
            glCompileShader(self.shaderid)
            
            if glGetShaderiv(self.shaderid, GL_COMPILE_STATUS) != GL_TRUE:
                raise RuntimeError(glGetShaderInfoLog(self.shaderid))
        
        def dispose(self):
            glDeleteShader(self.shaderid)
   
class ShaderProgram(object):
    def __init__(self):
        self.programid = glCreateProgram()
        
    def bind(self):
        glUseProgram(self.programid)
    
    def bindInput(self, index, name):
        glBindAttribLocation(self.programid, index, name)
    
    def attachShader(self, shader):
        glAttachShader(self.programid, shader.shaderid)
    
    def unbind(self):
        glUseProgram(0)
        
    def link(self):
        glLinkProgram(self.programid)
        
        if glGetProgramiv(self.programid, GL_LINK_STATUS) != GL_TRUE:
            raise RuntimeError(glGetProgramInfoLog(self.programid))
    
    def getUniformLocation(self, name):
        return glGetUniformLocation(self.programid, name)
    
    def bindUniformBlock(self, binding, name):
        index = glGetUniformLocation(self.programid, name)
        if index > -1:
            glUniformBlockBinding(self.programid, index, binding)
    
    def dispose(self):
        glDeleteProgram(self.programid)
       

# this is more accurately an EffectPass
class BasicEffect(object):
    def __init__(self, vertexshader, fragmentshader):
        self.shaderprogram = ShaderProgram()
        self.shaderprogram.attachShader(vertexshader)
        self.shaderprogram.attachShader(fragmentshader)
        # self.shaderprogram.bindInput(0, "vPosition")
        self.shaderprogram.link()
        
    def bind(self):
        self.shaderprogram.bind()
        
    def unbind(self):
        self.shaderprogram.unbind()