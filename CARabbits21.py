import numpy, random
import math
from CA import CA


class CARabbits(CA):
    """
    Rabbits weeds grass CA, version containing
    variable length grass, virus and fences
        
    Rd(16): Rabbit Age / Rabbit Energy
    Gr(16): 0 / Grass Length
    Bl(16): 0 / Weed Length
    Al(16): 0 / Special (8=breed),(10=wall)
    """

    def __init__(self, width, height, rabbits=10, grass=50):
        self.width = width
        self.height = height
        self.channels = 4
        self.type = numpy.uint16
        
        self.vsc = """
            #version 150
            in vec4 vPosition;
            out vec2 fTexCoord;
            void main() {
                fTexCoord = vec2(vPosition.x + 1, -vPosition.y + 1) / 2;
                gl_Position = vPosition;
            }
        """
        
        self.fsc = """
            #version 150

            in vec2 fTexCoord;
            out vec4 color;
            
            uniform sampler2D celldata;
            
            void main() {
                int max = 65535;
                int maxgrasslife = 255;
                int breedthreshold = 128;
                
                vec2 texcoord = fTexCoord;
                vec4 sample = texture(celldata, texcoord);
                
                ivec4 cell = ivec4(max * sample);
                float grasslife = (cell.y) / float(maxgrasslife);
                
                if(cell.r>0) {
                    if(cell.a == 8) color = vec4(1,0.5,0.5,1);
                    else if(cell.b > 0) color = vec4(0.5, 0, 1,1);
                    else color = vec4(1,1,1,1);
                }
                else if(cell.a == 10) color = vec4(1, 0.5, 0, 1);
                else color = vec4(0.3 - (0.1*grasslife), 0.2 + (0.9*grasslife), 0.2 - (0.1*grasslife), 1);
            }
        """

        self.total_rabbits = 0
        self.total_grass = 0


        # config
        self.max_grass_length = 255

        self.max_rabbit_age = 24
        self.rabbit_feed = 32
        self.rabbit_drain = 24

        self.breed_threshold = 128
        self.breed_drain = 60
        self.min_birth_energy = 32
        self.max_birth_energy = 32

        self.max_grass_growth = 2
        self.weed_growth = 1

        self.grass_energy = 1
        self.weed_energy = 1

        self.virus_chance = 0
        self.virus_life = 3

        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)
        self.nextcells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)

    # try 64, 44, 99, 60, 20 - swirl around an origin
    def controls(self):
        return [
            {'name': 'Max Grass Growth', 'attr': 'max_grass_growth'},
            {'name': 'Max Rabbit Age', 'attr': 'max_rabbit_age'},
            {'name': 'Rabbit Feed', 'attr': 'rabbit_feed'},
            {'name': 'Rabbit Drain', 'attr': 'rabbit_drain'},
            {'name': 'Birth Threshold', 'attr': 'breed_threshold'},
            {'name': 'Birth Drain', 'attr': 'breed_drain'},
            {'name': 'Min Birth Energy', 'attr': 'min_birth_energy'},
            {'name': 'Max Birth Energy', 'attr': 'max_birth_energy'},

            {'name': 'Virus Chance %', 'attr': 'virus_chance'},
            {'name': 'Virus Life Left', 'attr': 'virus_life'}

        ]

    def trackers(self):
        return [
            {'name': 'rabbits', 'attr': 'total_rabbits', 'color': 'red'},
            {'name': 'grass', 'attr': 'total_grass', 'color': 'green'}]

    def get_cell(self, x, y):
        return self.cells[y % self.height, x % self.width, :]

    def get_channel(self, x, y, z):
        return self.cells[y % self.height, x % self.width, z]

    def set_cell(self, x, y, value):
        self.nextcells[y % self.height, x % self.width, :] = value

    def set_channel(self, x, y, z, value):
        self.nextcells[y % self.height, x % self.width, z] = value

    def get_next_channel(self, x, y, z):
        return self.nextcells[y % self.height, x % self.width, z]
        
    def reset(self, rabbits=10, grass=120):
        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)
        self.nextcells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)

    def step(self):
        """Update next_cells from cells, then swap and clear the previous frame"""
        self.total_rabbits = 0
        self.total_grass = 0

        for x in range(self.width):
            for y in range(self.height):
                # copy rabbits
                # self.set_channel(x,y,0, self.get_channel(x,y,0))

                # copy walls and traps
                special = self.get_channel(x, y, 3)
                if special > 0:
                    self.set_channel(x, y, 3, special)

                # copy and grow grass
                grass = self.get_channel(x, y, 1)
                self.total_grass += (grass / 1024.0)
                # grow grass at exponential rate (with a bit of random)
                if grass < self.max_grass_length:
                    grass = min(self.max_grass_length, grass + random.randint(0, self.max_grass_growth))
                self.set_channel(x, y, 1, grass)

        # Alter rabbits
        for x in range(self.width):
            for y in range(self.height):

                rabbit = self.get_channel(x, y, 0)
                rabbit_age = rabbit / 256
                rabbit_energy = rabbit % 256

                virus_life = self.get_channel(x, y, 2)

                if (rabbit_energy > 0) & (rabbit_age < self.max_rabbit_age) & (virus_life < 2):

                    # infect rabbits

                    if virus_life == 0:
                        infection = (self.virus_chance > random.randint(0, 100))
                        if infection:
                            virus_life = 1
                    else:
                        virus_life += 1

                    rabbit_age += 1
                    self.total_rabbits += 1
                    directions = [[x,y]]

                    # detect if rabbit is already in an adjacent space
                    if (self.get_channel(x + 1, y, 0) == 0) & (self.get_next_channel(x + 1, y, 0) == 0) & (self.get_channel(x+1, y, 3) != 10):
                        directions.append([x + 1, y])
                    if (self.get_channel(x - 1, y, 0) == 0) & (self.get_next_channel(x - 1, y, 0) == 0) & (self.get_channel(x-1, y, 3) != 10):
                        directions.append([x - 1, y])
                    if (self.get_channel(x, y + 1, 0) == 0) & (self.get_next_channel(x, y + 1, 0) == 0) & (self.get_channel(x, y+1, 3) != 10):
                        directions.append([x, y + 1])
                    if (self.get_channel(x, y - 1, 0) == 0) & (self.get_next_channel(x, y - 1, 0) == 0) & (self.get_channel(x, y-1, 3) != 10):
                        directions.append([x, y - 1])
                    
                    d = random.choice(directions)

                    new_x = d[0]
                    new_y = d[1]

                    # rabbit feed
                    grass = int(self.get_channel(new_x, new_y, 1))
                    eaten = min(self.rabbit_feed, grass)
                    rabbit_energy = min(255, rabbit_energy + eaten)  # increase rabbit health
                    self.set_channel(new_x, new_y, 1, max(0, grass - eaten))  # decrease grass health
                    
                    # rabbit breed
                    if (rabbit_energy > self.breed_threshold) & (virus_life == 0):
                        rabbit_energy -= self.breed_drain
                        if self.get_channel(new_x - 1, new_y, 3) != 10:
                            self.set_channel(new_x - 1, new_y, 0, random.randint(self.min_birth_energy, self.max_birth_energy))
                        if self.get_channel(new_x + 1, new_y, 3) != 10:
                            self.set_channel(new_x+1, new_y, 0, random.randint(self.min_birth_energy, self.max_birth_energy))
                        if self.get_channel(new_x, new_y-1, 3) != 10:
                            self.set_channel(new_x, new_y-1, 0, random.randint(self.min_birth_energy, self.max_birth_energy))
                        if self.get_channel(new_x, new_y+1, 3) != 10:
                            self.set_channel(new_x, new_y+1, 0, random.randint(self.min_birth_energy, self.max_birth_energy))
                        
                    # drain rabbit
                    rabbit_energy = max(0, rabbit_energy - self.rabbit_drain)

                    # set rabbit
                    self.set_channel(new_x, new_y, 0, rabbit_age * 256 + rabbit_energy)
                    self.set_channel(new_x, new_y, 2, virus_life)

        # swap
        self.cells, self.nextcells = self.nextcells, self.cells
        self.nextcells.fill(0)

if __name__ == "__main__":    
    from CAViewerQt4 import CAViewerQt4
    ca = CARabbits(64, 64)
    viewer = CAViewerQt4(ca)
    viewer.run()
