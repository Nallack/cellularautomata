

from CARabbits import *
from CAViewerQt4 import *


class RabbitsWeedsGrassApp(object):
    """Run Rabbits Grass Weeds"""
    
    def __init__(self, width=64, height=64):
        self.ca = CARabbits(width, height)
        self.viewer = CAViewerQt4(self.ca)
    
    def run(self):
        self.viewer.run()
    
if __name__ == "__main__":    
    app = RabbitsWeedsGrassApp()
    app.run()
