import numpy, random
import math
from CA import CA


class CARabbits(CA):
    """
    Rabbits weeds grass CA 
        
    Rd(8): Rabbit Health
    Gr(8): Grass Length
    Bl(8): Weed Length
    Al(8): Special
    """

    def __init__(self, width, height, rabbits=10, grass=50):
        self.width = width
        self.height = height
        self.channels = 4
        self.type = numpy.uint8
        
        self.vsc = """
            #version 150

            in vec4 vPosition;
            out vec4 fPosition;
             
            void main() {
                fPosition = vPosition;
                gl_Position = vPosition;
            }
        """
        
        self.fsc = """
            #version 150

            in vec4 fPosition;
            out vec4 color;
            
            uniform sampler2D celldata;
            
            void main() {
                int max = 255;
                int maxgrasslife = 255;
                int breedthreshold = 128;
                
                vec2 texcoord = vec2(fPosition.x + 1, -fPosition.y + 1) / 2;
                vec4 sample = texture(celldata, texcoord);
                
                ivec4 cell = ivec4(255 * sample);
                float grasslife = (cell.y) / float(maxgrasslife);
                
                if(cell.r>0) {
                    if(cell.r>breedthreshold) color = vec4(1,0.5,0.5,1);
                    else color = vec4(1,1,1,1);
                }
                else               color = vec4(0.3 - (0.1*grasslife), 0.2 + (0.9*grasslife), 0.2 - (0.1*grasslife), 1);
                //else               color = vec4(0,0,0,1);
            }
        """

        self.total_rabbits = 0
        self.total_grass = 0

        self.max_grass_life = 255
        self.breed_threshold = 128
        self.breed_drain = 60
        self.grass_growth = 20

        self.rabbit_feed = 32
        self.rabbit_drain = 28

        self.reset()

    # try 64, 44, 99, 60, 20 - swirl around an origin
    def controls(self):
        return [
            {'name': 'Rabbit Feed', 'attr': 'rabbit_feed'},
            {'name': 'Rabbit Drain', 'attr': 'rabbit_drain'},
            {'name': 'Birth Threshold', 'attr': 'breed_threshold'},
            {'name': 'Birth Drain', 'attr': 'breed_drain'},
            {'name': 'Grass Growth', 'attr': 'grass_growth'}
        ]

    def trackers(self):
        return [
            {'name': 'rabbits', 'attr': 'total_rabbits', 'color': 'red'},
            {'name': 'grass', 'attr': 'total_grass', 'color': 'green'}]

    def get_cell(self, x, y):
        return self.cells[y % self.height, x % self.width, :]

    def get_channel(self, x, y, z):
        return self.cells[y % self.height, x % self.width, z]

    def set_cell(self, x, y, value):
        self.nextcells[y % self.height, x % self.width, :] = value

    def set_channel(self, x, y, z, value):
        self.nextcells[y % self.height, x % self.width, z] = value

    def get_next_channel(self, x, y, z):
        return self.nextcells[y % self.height, x % self.width, z]
        
    def reset(self, rabbits=10, grass=120):
        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)
        self.nextcells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)

    def step(self):
        """Update next_cells from cells, then swap and clear the previous frame"""
        self.total_rabbits = 0
        self.total_grass = 0

        for x in range(self.width):
            for y in range(self.height):
                # copy rabbits
                # self.set_channel(x,y,0, self.get_channel(x,y,0))
                
                # copy and grow grass
                grass = self.get_channel(x, y, 1)
                self.total_grass += (grass / 1024.0)
                # grow grass at exponential rate (with a bit of random)
                if grass < self.max_grass_life:
                    grass = min(self.max_grass_life, grass + random.randint(0, 2))  #  + math.exp(life / 128.0))
                self.set_channel(x, y, 1, grass)

        # Alter rabbits
        for x in range(self.width):
            for y in range(self.height):
                if self.get_channel(x, y, 0) > 0:         
                    self.total_rabbits += 1
                    directions = [0]
                    
                    # detect if rabbit is already in an adjacent space
                    if (self.get_channel(x + 1, y, 0) == 0) & (self.get_next_channel(x + 1, y, 0) == 0):
                        directions.append(1)
                    if (self.get_channel(x - 1, y, 0) == 0) & (self.get_next_channel(x - 1, y, 0) == 0):
                        directions.append(2)
                    if (self.get_channel(x, y + 1, 0) == 0) & (self.get_next_channel(x, y + 1, 0) == 0):
                        directions.append(3)
                    if (self.get_channel(x, y - 1, 0) == 0) & (self.get_next_channel(x, y - 1, 0) == 0):
                        directions.append(4)
                    
                    direction = random.choice(directions)
                    
                    d = [x, y]
                    if direction == 1:
                        d = [x + 1, y]
                    if direction == 2:
                        d = [x - 1, y]
                    if direction == 3:
                        d = [x, y + 1]
                    if direction == 4:
                        d = [x, y - 1]
                    
                    self.set_channel(d[0], d[1], 0, self.get_channel(x, y, 0))  # move rabbit
                    
                    # rabbit feed
                    rabbit_health = int(self.get_channel(x, y, 0))
                    grass = int(self.get_channel(d[0], d[1], 1))
                    eaten = min(self.rabbit_feed, grass)
                    rabbit_health = min(255, rabbit_health + eaten)  # increase rabbit health
                    self.set_channel(d[0], d[1], 1, max(0, grass - eaten))  # decrease grass health
                    
                    # rabbit breed
                    if rabbit_health > self.breed_threshold:
                        rabbit_health -= self.breed_drain
                        self.set_channel(x-1, y, 0, 32)
                        self.set_channel(x+1, y, 0, 32)
                        self.set_channel(x, y-1, 0, 32)
                        self.set_channel(x, y+1, 0, 32)
                        
                    # drain rabbit
                    rabbit_health = max(0, rabbit_health - self.rabbit_drain)
                    self.set_channel(d[0], d[1], 0, rabbit_health) # set rabbit health

        # swap
        self.cells, self.nextcells = self.nextcells, self.cells
        self.nextcells.fill(0)

if __name__ == "__main__":    
    from CAViewerQt4 import CAViewerQt4
    ca = CARabbits(64,64)
    viewer = CAViewerQt4(ca)
    viewer.run()