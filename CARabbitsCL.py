import pyopencl as cl
import numpy
from CA import CA
import os


class CARabbits(CA):
    """Rabbits using OpenCL"""

    def __init__(self, width, height, random=False):
        self.width = width
        self.height = height
        self.channels = 1
        self.type = numpy.uint8

        self.vsc = """
            #version 150
            in vec4 vPosition;
            out vec2 fTexCoord;
            void main() {
                fTexCoord = vec2(vPosition.x + 1, -vPosition.y + 1) / 2;
                gl_Position = vPosition;
            }
        """

        self.fsc = """
            #version 150
            in vec2 fTexCoord;
            out vec4 color;
            uniform sampler2D celldata;
            void main() {
                ivec4 cell = ivec4(255 * texture(celldata, fTexCoord));
                if(cell.x == 1) color = vec4(1,1,1,1);
                else color = vec4(0,0,0,1);
            }
        """

        self.clc = """
            #pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
            #define LOCK(a) atom_cmpxchg(a, 0, 1)
            #define UNLOCK(a) atom_xchg(a, 0)

            uint random(ulong* seed) {
                *seed = (*seed * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
                uint result = *seed >> 16;
                return result;
            }

            uint index(x, y) {
                return (y % get_global_size(1)) * get_global_size(0) * get_global_size(2)
                     + (x % get_global_size(0));
            }

            kernel void main(global uchar* a, global uchar* b, ulong seed) {
                uint x = get_global_id(0);
                uint y = get_global_id(1);
                uint z = get_global_id(2);
                uint id = index(x, y);

                //prime seed
                seed += id+y*x*(x-y*x);
                seed *= seed<<32^seed<<16|seed;

                if(a[id] == 1) {
                    //move to random position
                    int dirs[5];
                    dirs[0] = 0;
                    int len = 1;
                    if(a[index(x+1,y)] == 0) dirs[len++] = 1;
                    if(a[index(x-1,y)] == 0) dirs[len++] = 2;
                    if(a[index(x,y+1)] == 0) dirs[len++] = 3;
                    if(a[index(x,y-1)] == 0) dirs[len++] = 4;

                    //choose
                    int choice = random(&seed) % (len);

                    uint dx = x; uint dy = y;
                    if(dirs[choice] == 1) dx = x+1;
                    if(dirs[choice] == 2) dx = x-1;
                    if(dirs[choice] == 3) dy = y+1;
                    if(dirs[choice] == 4) dy = y-1;

                    uint did = index(dx,dy);
                    b[did] = 1;
                }
            }
        """

        self.zeros = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)
        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)

        # --- OPENCL SETUP -----
        self.platform = cl.get_platforms()
        self.devices = self.platform[0].get_devices(cl.device_type.DEFAULT)
        self.context = cl.Context(self.devices)
        self.queue = cl.CommandQueue(self.context)
        self.program = cl.Program(self.context, self.clc).build()

        mf = cl.mem_flags
        self.buffer0 = cl.Buffer(self.context, mf.READ_ONLY, self.cells.nbytes)
        self.buffer1 = cl.Buffer(self.context, mf.READ_WRITE, self.cells.nbytes)

    def reset(self, random=True):
        self.cells = numpy.zeros((self.height, self.width, self.channels), dtype=self.type)

    def step(self):
        cl.enqueue_write_buffer(self.queue, self.buffer0, self.cells).wait()
        cl.enqueue_write_buffer(self.queue, self.buffer1, self.zeros).wait()
        self.program.main(self.queue, self.cells.shape, None,
                          self.buffer0,
                          self.buffer1,
                          numpy.uint64(numpy.random.randint(0,1<<8)))
        cl.enqueue_read_buffer(self.queue, self.buffer1, self.cells).wait()


if __name__ == "__main__":
    from CAViewerQt4 import CAViewerQt4

    ca = CARabbits(128, 128)
    viewer = CAViewerQt4(ca)
    viewer.run()
