"""
@copyright: 2016, Callan Gray <nallack@nethersoft.com.au>
@license: BSD, see LICENSE for details
"""

# http://winfred-lu.blogspot.com.au/2012/09/writing-your-first-pyqt4-opengl-program.html

# check out this for core-profile support
# http://stackoverflow.com/questions/25708478/pyqt4-opengl-enabling-opengl-core-profile

import sys, os, sip
from netherpy.graphics import *

# try:
#    from PySide import QtGui, QtOpenGL
#    from PySide.QtCore import *
#    from PySide.QtOpenGL import *
# except ImportError:
from PyQt4 import Qt, QtGui, QtOpenGL, Qwt5
from PyQt4.QtCore import *


class CAViewerQt4(object):
    def __init__(self, ca):
        self.app = QtGui.QApplication(sys.argv)
        QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))
        self.window = CAWindow(ca)
        self.window.setWindowTitle('Cellular Automata')
        # self.window.resize(600, 600)
        # self.window.show()

    def run(self):
        # self.window.show()
        self.app.exec_()


class CAWindow(QtGui.QMainWindow):
    def __init__(self, ca):
        super(CAWindow, self).__init__()

        exit_action = QtGui.QAction(QtGui.QIcon('exit.png'), '&Exit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(QtGui.qApp.quit)

        # about_action = QtGui.QAction(QtGui.QIcon('exit.png'), '&About', self)
        license_action = QtGui.QAction(QtGui.QIcon(), '&License', self)
        license_action.setShortcut('F2')
        license_action.triggered.connect(self.show_license)

        self.setCentralWidget(CAWidget(ca))

        self.statusBar().showMessage('Ready')
        menu_bar = self.menuBar()

        file_menu = menu_bar.addMenu('File')
        file_menu.addAction(exit_action)
        help_menu = menu_bar.addMenu('Help')
        # help_menu.addAction(about_action)
        help_menu.addAction(license_action)

        self.resize(800, 400)
        self.setWindowTitle('Cellular Automata')
        self.show()

    def show_license(self):
        with open(os.path.join(sys.path[0], 'LICENSE')) as file:
            self.dialog = QtGui.QMessageBox.information(self, 'License', file.read())


class CAWidget(QtGui.QWidget):
    def __init__(self, ca):
        super(CAWidget, self).__init__()
        self.ca = ca

        self.caplotter = CAPlotWidget(self.ca)
        self.carender = CARenderWidget(self.ca, self.caplotter)
        self.cacontrols = CAControls(self.ca, self.carender, self.caplotter)

        # ----- SPLITTER LAYOUT ---------
        mainLayout = QtGui.QGridLayout()

        h_split = QtGui.QSplitter(Qt.Horizontal)
        h_split.addWidget(self.carender)
        h_split.addWidget(self.cacontrols)

        v_split = QtGui.QSplitter(Qt.Vertical)
        v_split.addWidget(h_split)
        v_split.addWidget(self.caplotter)
        # v_split.setSizes([1, 0])

        mainLayout.addWidget(v_split)
        self.setLayout(mainLayout)


class CAControls(QtGui.QWidget):
    def __init__(self, ca, carender, caplotter):
        super(CAControls, self).__init__()

        self.ca = ca
        self.carender = carender
        self.caplotter = caplotter

        # ---mode----
        self.modeBox = QtGui.QComboBox()
        self.modeBox.addItem("Game of Life")
        self.modeBox.addItem("Game of Life (CL)")
        self.modeBox.addItem("Rabbits (CL)")
        self.modeBox.addItem("Rabbits v1")
        self.modeBox.addItem("Rabbits v1.2")
        self.modeBox.addItem("Rabbits v1.3")
        self.modeBox.addItem("Rabbits v2")
        self.modeBox.addItem("Rabbits v2.1")
        self.modeBox.currentIndexChanged.connect(self.mode_changed)

        # ---standard controls----

        self.widthBox = QtGui.QSpinBox()
        self.widthBox.setRange(8, 2048)
        self.widthBox.setValue(64)
        self.heightBox = QtGui.QSpinBox()
        self.heightBox.setRange(8, 2048)
        self.heightBox.setValue(64)
        self.speedBox = QtGui.QSpinBox()
        self.speedBox.setRange(1, 60)
        self.speedBox.setValue(30)
        self.speedBox.valueChanged.connect(self.carender.setFramerate)

        self.brushRBox = QtGui.QSpinBox()
        self.brushRBox.setValue(0)
        self.brushRBox.setRange(0, 255)
        self.brushRBox.valueChanged.connect(self.carender.set_brush_r)
        self.brushGBox = QtGui.QSpinBox()
        self.brushGBox.setValue(0)
        self.brushGBox.setRange(0, 255)
        self.brushGBox.valueChanged.connect(self.carender.set_brush_g)
        self.brushBBox = QtGui.QSpinBox()
        self.brushBBox.setValue(0)
        self.brushBBox.setRange(0, 255)
        self.brushBBox.valueChanged.connect(self.carender.set_brush_b)
        self.brushABox = QtGui.QSpinBox()
        self.brushABox.setValue(0)
        self.brushABox.setRange(0, 255)
        self.brushABox.valueChanged.connect(self.carender.set_brush_a)

        self.stepButton = QtGui.QPushButton("Step")
        self.stepButton.clicked.connect(self.carender.updateCA)
        self.startButton = QtGui.QPushButton("Start/Stop")
        self.startButton.clicked.connect(self.carender.startstop)
        self.resetButton = QtGui.QPushButton("Reset")
        self.resetButton.clicked.connect(self.carender.reset_ca)
        self.clearButton = QtGui.QPushButton("Clear")
        self.clearButton.clicked.connect(self.carender.clearCA)

        self.layout = QtGui.QGridLayout()

        # --standard controls-----
        standardgroup = QtGui.QGroupBox("Standard Controls")
        slayout = QtGui.QGridLayout()
        dy = 0
        dx = 2
        slayout.addWidget(QtGui.QLabel("Mode"), dy, dx)
        slayout.addWidget(self.modeBox, dy, dx + 1)
        dy += 1
        slayout.addWidget(QtGui.QLabel("Width"), dy, dx)
        slayout.addWidget(self.widthBox, dy, dx + 1)
        dy += 1
        slayout.addWidget(QtGui.QLabel("Height"), dy, dx)
        slayout.addWidget(self.heightBox, dy, dx + 1)
        dy += 1
        slayout.addWidget(QtGui.QLabel("FPS"), dy, dx)
        slayout.addWidget(self.speedBox, dy, dx + 1)
        dy += 1
        slayout.addWidget(QtGui.QLabel("Brush"), dy, dx)
        slayout.addWidget(self.brushRBox, dy, dx + 1)
        dy += 1
        slayout.addWidget(self.brushGBox, dy, dx + 1)
        dy += 1
        slayout.addWidget(self.brushBBox, dy, dx + 1)
        dy += 1
        slayout.addWidget(self.brushABox, dy, dx + 1)
        dy += 1
        slayout.addWidget(self.stepButton, dy, dx, 1, 2)
        dy += 1
        slayout.addWidget(self.startButton, dy, dx, 1, 2)
        dy += 1
        slayout.addWidget(self.resetButton, dy, dx, 1, 2)
        dy += 1
        slayout.addWidget(self.clearButton, dy, dx, 1, 2)
        dy += 1
        slayout.setRowStretch(dy, 1)

        standardgroup.setLayout(slayout)
        self.layout.addWidget(standardgroup, 0, 2)

        self.cgroup = None
        self.update_custom_controls()

    def set_ca(self, ca):
        self.ca = ca
        self.carender.set_ca(ca)
        self.caplotter.set_ca(ca)
        self.update_custom_controls()

    def update_custom_controls(self):
        dy = 0
        dx = 0

        def make_setter(obj, attr):
            def setter(value):
                setattr(obj, attr, value)

            return setter

        if (self.cgroup != None):
            self.layout.removeWidget(self.cgroup)
            sip.delete(self.cgroup)

        self.cgroup = QtGui.QGroupBox("Custom Controls")
        clayout = QtGui.QGridLayout()
        for control in self.ca.controls():
            spinner = QtGui.QSpinBox()
            spinner.setValue(getattr(self.ca, control['attr']))
            callback = make_setter(self.ca, control['attr'])
            spinner.setRange(0, 255)
            spinner.valueChanged.connect(callback)

            clayout.addWidget(QtGui.QLabel(control['name']), dy, dx)
            clayout.addWidget(spinner, dy, dx + 1)
            dy += 1
        clayout.setRowStretch(dy, 1)
        self.cgroup.setLayout(clayout)
        self.layout.addWidget(self.cgroup, 0, 0)
        self.setLayout(self.layout)

    def mode_changed(self, index):
        if index == 0:
            from CALife import CALife
            self.ca = CALife(
                self.widthBox.value(),
                self.heightBox.value(),
                random=True)
            self.set_ca(self.ca)
        if index == 1:
            from CALifeCL import CALife
            self.ca = CALife(
                self.widthBox.value(),
                self.heightBox.value(),
                random=True)
            self.set_ca(self.ca)
        if index == 2:
            from CARabbitsCL import CARabbits
            self.ca = CARabbits(
                self.widthBox.value(),
                self.heightBox.value())
            self.set_ca(self.ca)
        if index == 3:
            from CARabbits import CARabbits
            self.ca = CARabbits(
                self.widthBox.value(),
                self.heightBox.value())
            self.set_ca(self.ca)
        if index == 4:
            from CARabbits12 import CARabbits
            self.ca = CARabbits(
                self.widthBox.value(),
                self.heightBox.value())
            self.set_ca(self.ca)
        if index == 5:
            from CARabbits13 import CARabbits
            self.ca = CARabbits(
                self.widthBox.value(),
                self.heightBox.value())
            self.set_ca(self.ca)
        if index == 6:
            from CARabbits2 import CARabbits
            self.ca = CARabbits(
                self.widthBox.value(),
                self.heightBox.value())
            self.set_ca(self.ca)
        if index == 7:
            from CARabbits21 import CARabbits
            self.ca = CARabbits(
                self.widthBox.value(),
                self.heightBox.value())
            self.set_ca(self.ca)


class CAPlotWidget(Qwt5.QwtPlot):
    def __init__(self, ca, *args):
        Qwt5.QwtPlot.__init__(self)
        self.setCanvasBackground(Qt.white)
        self.x = numpy.arange(0.0, 100.0, 1.0)

        # self.y = numpy.zeros(len(self.x), numpy.float)
        self.y = numpy.arange(0.0, 100.1, 0.5)
        self.z = numpy.zeros(len(self.x), numpy.float)

        self.curves = None
        self.set_ca(ca)
        self.setTitle("Populations")
        self.insertLegend(Qwt5.QwtLegend(), Qwt5.QwtPlot.BottomLegend)

    def set_ca(self, ca):
        self.ca = ca
        self.time = 0
        self.x = numpy.arange(0, 1000, 1)

        if self.curves is not None:
            for name in self.curves:
                curve = self.curves[name]['curve']
                curve.detach()
                # sip.delete(curve)

        self.curves = {}
        for tracker in ca.trackers():
            curve = Qwt5.QwtPlotCurve(tracker['name'])
            data = numpy.zeros(1000)
            color = tracker['color']
            if color == 'red':
                curve.setPen(QtGui.QPen(Qt.red))
            elif color == 'green':
                curve.setPen(QtGui.QPen(Qt.green))
            elif color == 'orange':
                curve.setPen(QtGui.QPen(QtGui.QColor(255, 127, 0)))
            else:
                curve.setPen(QtGui.QPen(Qt.black))
            curve.setData(self.x, data)
            curve.attach(self)
            self.curves[tracker['name']] = {"curve": curve, "data": data}

    def step(self):
        self.time += 1
        for tracker in self.ca.trackers():
            curve = self.curves[tracker['name']]['curve']
            data = self.curves[tracker['name']]['data']
            data[self.time % 1000] = getattr(self.ca, tracker['attr'])
            curve.setData(self.x, data)
            curve.attach(self)

        self.replot()


class CARenderWidget(QtOpenGL.QGLWidget):
    def __init__(self, ca, plotter, parent=None):
        glformat = QtOpenGL.QGLFormat()
        glformat.setVersion(3, 2)
        super(CARenderWidget, self).__init__(glformat, parent=parent)

        self.ca = ca
        self.plotter = plotter
        self.framerate = 30.0
        self.brush = [0, 0, 0, 0]

        self.timer = QTimer()
        self.timer.timeout.connect(self.updateCA)
        self.running = False

    def set_brush_r(self, value):
        self.brush[0] = value

    def set_brush_g(self, value):
        self.brush[1] = value

    def set_brush_b(self, value):
        self.brush[2] = value

    def set_brush_a(self, value):
        self.brush[3] = value

    def mousePressEvent(self, e):
        self.mouseMoveEvent(e)

    def mouseMoveEvent(self, e):

        x = int(float(e.x()) / self.width() * self.ca.width)
        y = int(float(e.y()) / self.height() * self.ca.height)

        if (0 <= x & x < self.ca.width) & (0 <= y & y < self.ca.height):
            if self.ca.channels > 0:
                self.ca.cells[y, x, 0] = self.brush[0]
            if self.ca.channels > 1:
                self.ca.cells[y, x, 1] = self.brush[1]
            if self.ca.channels > 2:
                self.ca.cells[y, x, 2] = self.brush[2]
            if self.ca.channels > 3:
                self.ca.cells[y, x, 3] = self.brush[3]

            self.texture.update(x, y, 1, 1, self.ca.cells[y, x])
            self.update()

    def setFramerate(self, framerate):
        self.framerate = framerate
        if (self.running):
            self.timer.stop()
            self.timer.start(1000.0 / self.framerate)

    def startstop(self):
        if (self.running):
            self.timer.stop()
        else:
            self.timer.start(1000.0 / self.framerate)
        self.running = ~self.running

    def set_ca(self, ca):
        self.ca = ca

        vertexshader = Shader(GL_VERTEX_SHADER)
        vertexshader.compileFromString(self.ca.vsc)

        fragmentshader = Shader(GL_FRAGMENT_SHADER)
        fragmentshader.compileFromString(self.ca.fsc)
        self.basiceffect = BasicEffect(vertexshader, fragmentshader)

        self.texture = Texture2D(self.ca.width,
                                 self.ca.height,
                                 self.ca.cells,
                                 channels=self.ca.channels,
                                 type=self.ca.type)

        self.update()

    def updateCA(self):  # self.update already defined
        self.ca.step()
        self.texture.update(0, 0, self.ca.width, self.ca.height, self.ca.cells)
        self.update()
        self.plotter.step()

    def reset_ca(self):
        self.ca.reset()
        self.texture.update(0, 0, self.ca.width, self.ca.height, self.ca.cells)
        self.update()

    def clearCA(self):
        self.ca.cells.fill(0)
        self.texture.update(0, 0, self.ca.width, self.ca.height, self.ca.cells)
        self.update()

    def initializeGL(self):

        vertexshader = Shader(GL_VERTEX_SHADER)
        vertexshader.compileFromString(self.ca.vsc)

        fragmentshader = Shader(GL_FRAGMENT_SHADER)
        fragmentshader.compileFromString(self.ca.fsc)
        self.basiceffect = BasicEffect(vertexshader, fragmentshader)

        self.quad = Quad()

        self.texture = Texture2D(self.ca.width,
                                 self.ca.height,
                                 self.ca.cells,
                                 channels=self.ca.channels,
                                 type=self.ca.type)

    def paintGL(self):
        self.texture.bind(0)

        glClearColor(0.3, 0.3, 0.3, 1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        self.basiceffect.bind()
        self.quad.bind()
        self.quad.draw()
        self.quad.unbind()
        self.basiceffect.unbind()
        # self.swapBuffers()

    def resizeGL(self, w, h):
        glViewport(0, 0, w, h)


def main():
    from CALife import CALife
    app = QtGui.QApplication(sys.argv)
    window = CAWindow(CALife(256, 256, random=True))
    app.exec_()


# needed to call this way to allow window to close via code
if __name__ == "__main__":
    main()
