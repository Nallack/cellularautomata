# Cellular Automata in Python #

# Intro #

This project is aimed to allow for multiple cellular automata models to be simulated, visualized and plotted in a clean way.

This was created as part of a UWA CITS4403 course project.

[Online Folder](https://studentuwaeduau-my.sharepoint.com/personal/21308586_student_uwa_edu_au/Documents/CITS4403)

# Setup #

make sure you have installed the following dependencies:

* numpy
* scipy
* pyOpenGL
* ~~pygame~~
* PyQt4
* PyQwt

# Running #

Atm entry point is in RabbitGrassWeeds.py or CAVeiwerQt4.py (most CA files have an entry point configured as well)

For some simulations, some intervential with the brush will be needed to get them started.

### Rabbits 1.* ###
* Paint rabbits with a brush value of [1,0,0,0]

### Rabbits 2.* ###
* Paint healthy rabbits with a brush value of [50,0,0,0]
* Paint fences with a brush value of [0,0,0,10]