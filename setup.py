from distutils.core import setup
import py2exe
import numpy
from PyQt4 import *
from OpenGL import *

setup(
    console=['CAViewerQt4.py'],
    options={
        "py2exe": {
            "excludes": ["OpenGL"]
        }
    }
)